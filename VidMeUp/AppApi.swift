//
//  AppApi.swift
//  VidMeUp
//
//  Created by Архип on 4/13/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import Foundation

class  AppApi {
    private static let sharedInstance: AppApi? = AppApi();
    class var shared:AppApi {
        get{
            return self.sharedInstance!;
        }
        
}
    let client = HttpClient.shared
    let mainPath = "https://api.vid.me/"
    let mainVideoPath = "https://api.vid.me/videos/"
    let mainAuthPath = "https://api.vid.me/auth/"
    enum HttpMethods: String {
        case get = "GET"
        case post = "POST"
    }
    private func getVideosFromJsonDict(jsonDict: [String: AnyObject]?) -> [Video]{
        var videos = [Video]()
        if let json = jsonDict {
            if  let videosDA = json["videos"] as? [[String: AnyObject?]]
            {
                for var video in videosDA {
                    
                    let _title = video["title"] as? String
                    let _url = video["complete_url"] as? String
                    let _likes = video["likes_count"] as? Int
                    let _imageUrl = video["thumbnail_url"] as? String
                    let _width = video["width"] as? Int
                    let _height = video["height"] as? Int
                    
                    if _title == nil || _url == nil || _imageUrl == nil || _likes == nil || _width == nil || _height == nil {
                        continue
                    }
                    
                    let newVideo = Video(url: _url!, title: _title!, likes: _likes!, imageUrl: _imageUrl!, width: _width!, height: _height!)
                    videos.append(newVideo)
                }
                
                
            }}
        print (videos.count)
        return videos
    }
    func loadFeaturedVideos(limit: Int = 10 ,offset: Int = 0 , callBack: @escaping (_ videos: [Video])->Void){
        print ("p1 ", offset, "  ",limit )
        var url = "\(mainVideoPath)featured"
        
            url += "?offset=\(offset)&limit=\(limit)"
        
        let method = HttpMethods.get.rawValue
        client.loadVideos(url: url, method: method, headers: nil, data: nil) { (jsonDict) in
            
            callBack(self.getVideosFromJsonDict(jsonDict: jsonDict))
            
        }
    }

    func loadNewestVideos(limit:Int = 10 , offset: Int = 0 , callBack: @escaping (_ videos: [Video])->Void){
        print ("p2 ", offset, "  ",limit )
        var url = "\(mainVideoPath)new"
        
            url += "?offset=\(offset)&limit=\(limit)"
        
        let method = HttpMethods.get.rawValue
        client.loadVideos(url: url, method: method, headers: nil, data: nil) { (jsonDict) in
            //parse and invoke callback with videos objects
            callBack(self.getVideosFromJsonDict(jsonDict: jsonDict))
        }
    }
    func loadFeedVideos(limit:Int = 10 , offset: Int = 0 , callBack: @escaping (_ videos: [Video], _ status: Int)->Void){
        print ("p3 ", offset, "  ",limit )
        checkToken() {
            
            result in
            if (result) {
        var url = "\(self.mainVideoPath)feed"
        
        url += "?offset=\(offset)&limit=\(limit)"
        
        let method = HttpMethods.get.rawValue
        self.client.loadVideos(url: url, method: method, headers: nil, data: nil) { (jsonDict) in
            //parse and invoke callback with videos objects
            callBack(self.getVideosFromJsonDict(jsonDict: jsonDict), 1)
        }
            }
            else {
                callBack( [Video]() , 0 )
                
            }
            
            
        }
    }
    func logIn(login: String?, password: String?, callBack: @escaping (_ response: [String : AnyObject]?, _ statusCode: Int?)->Void){
      
        let postString = "username=\(login ?? "  ")&password=\(password ?? " " )"
        let data = postString.data(using: .utf8)
        let url = mainAuthPath + "create?" + postString
        client.logIn(url: url, method: HttpMethods.post.rawValue, headers: nil, _data: data) { (data, statusCode) in
            if (statusCode == 200){
                    
                User.shared.login = login
                User.shared.token = data?["auth"]?["token"] as? String
            }
        
            callBack(data, statusCode)
            
        }
    
    }
    //https://api.vid.me/auth/delete
    func logOut(token: String?, callBack: @escaping (_ result: Bool?, _ error: String?)->Void){
        
        let postString = "token=\(token ?? "  ")"
        let data = postString.data(using: .utf8)
        let url = mainAuthPath + "delete?" + postString
        generalHttpRequest(urlString: url, httpMethod: HttpMethods.post.rawValue, headers: nil , httpBody: nil) { (data, headers, statusCode, error) in
            if (statusCode == 200){
                callBack(true, nil)
                return
                
            }
            var response : [String : AnyObject]?
            if data != nil {
                if let jsonObject  = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String : AnyObject] {
                    response = jsonObject
                    }
            }
            
            callBack(false, response?["error"] as? String)
            
        }
        
    }
    
    func checkToken(callBack: @escaping (_ result: Bool)->Void){
        
        let url = "https://api.vid.me/auth/check"
        guard let token = User.shared.token else {
            callBack(false)
            return
        }
        let header = ["AccessToken" : token ]
        
        generalHttpRequest(urlString: url, httpMethod: HttpMethods.post.rawValue, headers: header , httpBody: nil) { (data, headers, statusCode, error) in
            if (error != nil ) {
                callBack(false)
                return
            }
            else {
                if (statusCode == 200) {
                    var response : [String : AnyObject]?
                    if data != nil {
                        if let jsonObject  = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String : AnyObject] {
                            response = jsonObject
                            
                            guard let result = response?["status"] as? Bool else {
                                callBack(false)
                                return
                            }
                            
                            if (result == true )
                            {
                                callBack(true)
                                return
                            }
                        }
                    }
                }
                
             callBack (false)
                
            }
            
        }
        
    }
   
    
}
