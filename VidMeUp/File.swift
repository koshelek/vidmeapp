//
//  File.swift
//  VidMeUp
//
//  Created by Архип on 4/13/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import Foundation
import UIKit

func generalHttpRequest (timeOut : Double = 0.0, urlString:String,httpMethod:String?, headers:[String:String]?, httpBody: Data?, callBack: ((_ resData:Data?, _ headersFields: [String:Any]?, _ statusCode: Int?, _ error:Error?)->Void)?){

    //if no internet
    if Reachability.isConnectedToNetwork() == false {
        (UIApplication.shared.delegate as? AppDelegate)?.noInternet()
        callBack?(nil, nil, nil, nil)
        return
    }
    
    let url = URL(string: urlString)!
    let session = URLSession.shared
    var request = URLRequest(url: url)
    if timeOut != 0.0 {
        request.timeoutInterval = timeOut
    }
    request.httpMethod = httpMethod
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    
    if headers != nil {
        for  (head,value) in headers! {
            request.addValue(value, forHTTPHeaderField: head)
        }
    }
    
    request.httpBody = httpBody
    let task = session.dataTask(with: request as URLRequest) {
        (data, response, error) in
        if (error != nil) {
            
            callBack?(nil, nil , nil, error )
        }
        let httpResponse = response as? HTTPURLResponse
        let statusCode = httpResponse?.statusCode
        let headers = httpResponse?.allHeaderFields as? [String:Any]
        
        
        
        callBack?(data,headers,statusCode,nil)
        
    }
    task.resume()
}
extension UIViewController {
func showMessage(message: String, title: String) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    
    let dismissAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
    }
    
    alertController.addAction(dismissAction)
    
    
    self.present(alertController, animated: true, completion: nil)
}
}
