//
//  FirstViewController.swift
//  VidMeUp
//
//  Created by Архип on 4/13/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, DataLoaderProtocol {
    @IBInspectable var typeOfController: Int = 0
    
    @IBOutlet weak var videoTableView: UITableView!
    var videoDataSource: VideoTableManagerData!
    var tableController: UITableViewController?  // this for pull down refresh from ios 9
    var offset = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        videoDataSource = VideoTableManagerData()
        self.videoTableView.dataSource = videoDataSource
        self.videoTableView.delegate   = videoDataSource
        self.videoDataSource.ownerViewController = self
        self.videoDataSource.delegate = self
        //self.videoDataSource.tableView = self.videoTableView
        self.videoTableView.tableFooterView = UIView(frame: .zero)
        self.videoTableView.register(UINib(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        
        
        self.tableController = UITableViewController()
        
        self.tableController?.tableView = self.videoTableView
        
        self.addChildViewController(self.tableController!)
        self.tableController?.didMove(toParentViewController: self)
        
        
        let refreshControl = UIRefreshControl()
        self.tableController?.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(self.refreshVideos), for: .valueChanged)

        
        
        self.refreshVideos()
            }
    
    
    func loadVideos(){
        let callBack = {(_ videos: [Video]) in
            DispatchQueue.main.async {
                [unowned self] in
                
                if (self.offset == 0){
                    self.videoDataSource.videos = videos
                    self.offset = videos.count
                } else {
                    self.videoDataSource.videos.append(contentsOf: videos)
                    self.offset = self.videoDataSource.videos.count
                }
                self.tableController?.refreshControl?.endRefreshing()
                self.videoTableView.reloadData()
            }
        }
        if (typeOfController == 0){
            AppApi.shared.loadFeaturedVideos(offset: self.offset){
                videos in
                callBack(videos)
            }
        } else if (typeOfController == 1){
            AppApi.shared.loadNewestVideos(offset: self.offset,callBack: { (videos) in
                callBack(videos)
            })
        } else if (typeOfController == 2){
            AppApi.shared.loadFeedVideos(offset: self.offset,callBack: { (videos, status) in
                callBack(videos)
            })
        }
        

    }
    func refreshVideos(){
        
        self.offset = 0
        self.tableController?.refreshControl?.beginRefreshing()
        self.loadVideos()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ImageLoader.shared.cache.removeAllObjects()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        videoDataSource?.label?.frame = CGRect(x: 0, y: videoTableView.frame.height * 0.1, width: videoTableView.frame.width, height: 20)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        for cell in self.videoTableView.visibleCells {
            (cell as? VideoTableViewCell)?.stop()
        }
    }
    


}

