//
//  HttpClient.swift
//  VidMeUp
//
//  Created by Архип on 4/13/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import Foundation

class  HttpClient {
    private static let sharedInstance: HttpClient? = HttpClient();
    class var shared:HttpClient {
        get{
            return self.sharedInstance!;
        }
        
    }
    let appToken = "Authorization: EWOzFx0pRhcTlyds00XJ2X68czSgsPfW:CWbwKgiVdXrtiBEys9zI2TLh1QJ6Tdwe6VqgZxc"
    
    func loadVideos(url: String, method: String, headers:[String : String]?, data: [String : AnyObject]?,  callBack: @escaping (_ jsonDict: [String: AnyObject]?)-> Void){
        var _data: Data?
        if (data != nil){
        let valid = JSONSerialization.isValidJSONObject(data!)
        if valid == false  {
            print ("bad data for JSONSerialization")
            callBack(nil)
            return
        }
        
        _data   = try? JSONSerialization.data(withJSONObject: data!, options: .prettyPrinted)
        }
        generalHttpRequest(urlString: url, httpMethod: method, headers: headers, httpBody: _data) { (data, headers, statusCode, error) in
            print ("result: of laoding  video \n with url: \(url)")
            if error != nil {
                callBack(nil)
                print ("error: \(error.debugDescription)")
            }else{
            var response : [String : AnyObject]?
            if data != nil {
                if let jsonObject  = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String : AnyObject] {
                    response = jsonObject
                    
                }
            }
                //print ("status code: \(statusCode)")
            if statusCode == 200 {
                callBack(response)
            }else {
                callBack(nil)
            }
            }
            print ("--------------------")
        }
        
    }
    
    func logIn(url: String, method: String, headers:[String : String]?, _data: Data?,  callBack: @escaping (_ jsonDict: [String: AnyObject]?, _ statusCode: Int?)-> Void){

                generalHttpRequest(urlString: url, httpMethod: method, headers: headers, httpBody: _data) { (data, headers, statusCode, error) in
            print ("result: of log in")
            if error != nil {
                callBack(nil,statusCode)
                print ("error: \(error.debugDescription)")
            }else{
                print ("status Code: \(statusCode)")
                var response : [String : AnyObject]?
                if data != nil {
                    if let jsonObject  = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String : AnyObject] {
                        response = jsonObject
                        
                    }
                }
                
                if statusCode == 200 {
                    callBack(response, statusCode)
                }else {
                    callBack(response,statusCode)
                }
            }
            print ("--------------------")
        }

        
    }
    
    
}
