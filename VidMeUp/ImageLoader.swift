//
//  ImageLoader
//  VidMeUp
//
//  Created by Архип on 4/13/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import Foundation
import UIKit
class ImageLoader: NSObject {
    
    private static let sharedInstance: ImageLoader? = ImageLoader();
    class var shared:ImageLoader {
        get{
            return self.sharedInstance!;
        }
        
    }
    var cache: NSCache<AnyObject, AnyObject>!
    var task: URLSessionDownloadTask!
    
    var session: URLSession!
    private override init(){
        super.init()
        cache = NSCache()
        session = URLSession.shared
        task = URLSessionDownloadTask()
    }
    func addImage(url: String, image: UIImage ){
        self.cache.setObject(image, forKey: url as AnyObject)
    }
    func loadImage(url: String, _callBack: @escaping(_ image: UIImage?, _ _url : String)->Void){
        if let image = self.cache.object(forKey: url as AnyObject) as? UIImage {
            _callBack(image, url)
            return
        }else {
            let artworkUrl = url
            print (url)
            let urll:URL! = URL(string: artworkUrl)!
            task = session.downloadTask(with: urll, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: urll){
                            let img:UIImage! = UIImage(data: data)
                        
                            self.cache.setObject(img, forKey: url as AnyObject)
                        _callBack(img, url)
                    
                        
                    
                }else {_callBack(nil, url)}
            })
            task.resume()
        }
    }
    
    
}
