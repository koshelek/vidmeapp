//
//  LogInViewController.swift
//  VidMeUp
//
//  Created by Архип on 4/14/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var stopEditing: UITapGestureRecognizer!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginTextField.delegate = self
        passwordTextField.delegate = self
        
        loginTextField.text = User.shared.login
        
        stopEditing.addTarget(self, action: #selector(LogInViewController._stopEditing))
        logInButton.addTarget(self, action: #selector(LogInViewController.logInAction), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    func logInAction (){
        AppApi.shared.logIn(login: loginTextField.text, password: passwordTextField.text) { (data, statusCode) in
            DispatchQueue.main.async {
                [unowned self] in
            if statusCode == 200 {
                self.view.removeFromSuperview()
            }
            //
            else if let error = data?["error"] as? String {
                self.showMessage(message: error, title: "Log In")
            }
                if statusCode == nil {
                    self.showMessage(message: "Check internet!", title: "Log In")
                }
            }
        }
       
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            passwordTextField.becomeFirstResponder()
            return true
        }
        else {
            textField.resignFirstResponder()
            return false
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.backgroundColor = UIColor.clear
        
        
    }
    func logIn(){
        if (loginTextField.text?.isEmpty == true){
            
            return
        }
        if (passwordTextField.text?.isEmpty == true){
            
            return
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func _stopEditing() {
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
