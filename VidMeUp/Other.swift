//
//  Other.swift
//  VidMeUp
//
//  Created by Архип on 4/13/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import AVKit
protocol DataLoaderProtocol {
    func loadVideos();
    
}
class VideoTableManagerData :NSObject, UITableViewDataSource,UITableViewDelegate
{
    public var videos: [Video] = [Video]()
    public weak var ownerViewController: UIViewController?
    public var delegate: DataLoaderProtocol?
    public var label: UILabel?
    //public weak var tableView: UITableView?
    
    
    override init() {
        super.init()
    }
    convenience init(videos: [Video]) {
        self.init()
        self.videos = videos
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         
        return videos[indexPath.row].heightFor100Width
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return videos.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        if videos.count == 0 {
            if label == nil {
                label = UILabel()
                
                label?.textAlignment = .center
                label?.contentMode = .center
                label?.text = "Pull down!"
                tableView.addSubview(label!)
                
            }
            label?.isHidden = false
            
            return 0
        }
        label?.isHidden = true
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoTableViewCell
        cell.video = videos[indexPath.row]
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (videos.count - 1  == indexPath.row){
        self.delegate?.loadVideos()
        }
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = videos[indexPath.row].url
        
            let videoURL = NSURL(string: url)
            let player = AVPlayer(url: videoURL! as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.ownerViewController?.present(playerViewController, animated: true) {
                playerViewController.player!.play()}
        
}
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.startVideo(scrollView)

            }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            self.startVideo(scrollView)
           }
    func startVideo(_ scrollView: UIScrollView){
        if let table = scrollView as? UITableView {
            for cell  in (table.visibleCells as? [VideoTableViewCell])! {
                let rectOfScreen = UIScreen.main.bounds
                let centerOfScreen = CGPoint(x: rectOfScreen.width/2 - 25, y: rectOfScreen.height/2 - 25)
                let rect = table.convert(cell.frame, to: table.superview!)
                
                
                if rect.contains(centerOfScreen) {
                    cell.play()
                }
                else {
                    cell.stop()
                }
            }
        }
    }
    
}
