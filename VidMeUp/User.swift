//
//  User.swift
//  VidMeUp
//
//  Created by Архип on 4/14/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import Foundation
enum userData: String {
    case UserToken = "UserToken"
    case Login = "Login"
    //case Password = "Password"
    
}

class  User {
    private static let sharedInstance: User? = User();
    
    class var shared:User {
        get{
            return self.sharedInstance!;
        }}

    
    var token: String?{
        get {
            return UserDefaults.standard.object(forKey: userData.UserToken.rawValue) as? String
        }
        set (newValue) {
            
            UserDefaults.standard.set(newValue, forKey: userData.UserToken.rawValue)
        }
    }
    var login: String?{
        get {
            return UserDefaults.standard.object(forKey: userData.Login.rawValue) as? String
        }
        set (newValue) {
            UserDefaults.standard.set(newValue, forKey: userData.Login.rawValue)
        }
    }
    
    
}
