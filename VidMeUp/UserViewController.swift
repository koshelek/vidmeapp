//
//  UserViewController.swift
//  VidMeUp
//
//  Created by Архип on 4/14/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    
    var logInController: LogInViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logInController = LogInViewController(nibName: "LogInViewController", bundle: Bundle.main)
        logInController.view.frame = self.view.bounds
        
        logOut.addTarget(self, action: #selector(UserViewController._logOut), for: .touchUpInside)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        logInController.view.setNeedsLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print ("token - \(User.shared.token ?? "null" )")
        if (User.shared.token != nil){
        AppApi.shared.checkToken(){
            result in
            if result == true {
                
            }else {
                DispatchQueue.main.async {
                    [unowned self] in
                self.view.addSubview(self.logInController.view)
                }
            }
            }
        } else {
            self.view.addSubview(logInController.view)
        }
        
    }
    

    @IBOutlet weak var firstCiewContrlContainer: UIView!
    @IBOutlet weak var logOut: UIButton!
    func _logOut(){
        for contr in self.childViewControllers {
            if  let vidContr = contr as? FirstViewController {
                for cell in vidContr.videoTableView.visibleCells {
                    (cell as? VideoTableViewCell)?.stop()
                }
            }
        }
        AppApi.shared.logOut(token: User.shared.token) { (boolR, error) in
            DispatchQueue.main.async {
               [unowned self] in
            
            if (error != nil) {
                
                self.showMessage(message: error!, title: "Log Out!")
            }
            else {
                self.view.addSubview(self.logInController.view)
                
            }
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
