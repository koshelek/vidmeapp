//
//  Video.swift
//  APP
//
//  Created by Архип on 11/29/16.
//  Copyright © 2016 ArkhypKoshel. All rights reserved.
//

import UIKit

class Video: NSObject {
    var url: String
    var title: String
    var imageUrl: String
    var likes: Int
    var heightFor100Width: CGFloat
    var width: Int
    var height: Int
    
    init(url: String, title:String, likes: Int, imageUrl: String, width: Int, height:Int) {
        self.url = url
        self.likes = likes
        self.title = title
        self.imageUrl = imageUrl
        self.width = width
        self.height = height
        self.heightFor100Width = CGFloat(Int(UIScreen.main.bounds.width) * height  / width + 26)
        //print ("width = \(width) : height = \(height) : heightFor100Width = \(heightFor100Width) : width = \(UIScreen.main.bounds.width)")
    }
    
    override func copy() -> Any {
        return Video(url: self.url, title: self.title, likes: self.likes, imageUrl: self.imageUrl, width: self.width, height:self.height)
    }
}
