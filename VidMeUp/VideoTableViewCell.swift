//
//  VideoTableViewCell.swift
//  VidMeUp
//
//  Created by Архип on 4/13/17.
//  Copyright © 2017 ArkhypKoshel. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoTableViewCell: UITableViewCell {
    var video: Video? {
        didSet{
            self.setVideo()
        }
    }
    
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var videoName: UILabel!
    @IBOutlet weak var videoImage: UIImageView!
    var activityInd : UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityInd = UIActivityIndicatorView(activityIndicatorStyle: .white)
        //activityInd.bounds = videoImage.bounds
        
        activityInd.hidesWhenStopped = true
        self.addSubview(activityInd)
        
       
    }
    
    func setVideo(){
        activityInd.center = CGPoint(x: UIScreen.main.bounds.width/2, y: self.video!.heightFor100Width / 2);
        
        self.stop()
        self.videoName.text = self.video?.title
        self.likeLabel.text = "\(self.video!.likes) likes"
        self.videoImage.image = nil
        self.activityInd.startAnimating()
        if let lUrl = self.video?.imageUrl {
        ImageLoader.shared.loadImage(url: lUrl) { (image, url) in
            DispatchQueue.main.async {
                [unowned self] in
            
            if image != nil {
                if self.video?.imageUrl == url
                {
                    self.videoImage.image = image
                }
            }
                self.activityInd.stopAnimating()
            }
        }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var playerContoller = AVPlayerViewController()
    var playStatus = 0
    func play(){
        if playStatus == 0 {
        let url = self.video?.url
        if url != nil {
            playerContoller.view.frame = videoImage.frame
            let videoURL = NSURL(string: url!)
            let player = AVPlayer(url: videoURL! as URL)
             self.addSubview(playerContoller.view)
            playerContoller.player = player
                playerContoller.player!.play()
            playStatus = 1
        }
        }
    }
    func stop(){
        playerContoller.view.removeFromSuperview()
        playerContoller.player?.pause()
        playerContoller.player = nil
        playStatus = 0 
    }
    func pause(){
        
    }
}
